package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NestedFramesPO {
    public WebDriver driver;
    public WebDriverWait wait;

    public NestedFramesPO(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
    }

    public WebElement getFrameLeft() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body[contains(text(),'LEFT')]")));
    }

    public WebElement getFrameMiddle() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));
    }

    public WebElement getFrameRight() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body[contains(text(),'RIGHT')]")));
    }

    public WebElement getFrameBottom() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//body[contains(text(),'BOTTOM')]")));
    }
}
