package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginSauceLabPO {
    public WebDriver driver;
    public WebDriverWait wait;

    public LoginSauceLabPO(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
    }

    public WebElement setUsernameTextField() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user-name")));
    }

    public WebElement setPasswordTextField() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
    }

    public WebElement getLoginButton() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("btn_action")));
    }
}
