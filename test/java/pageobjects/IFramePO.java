package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IFramePO {
    public WebDriver driver;
    public WebDriverWait wait;

    public IFramePO(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
    }

    public WebElement getTinyMCETextField() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'An iFrame containing the TinyMCE WYSIWYG Editor')]")));
    }

    public WebElement getFormatsButton() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mceu_2-open")));
    }
}