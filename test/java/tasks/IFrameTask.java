package tasks;

import org.openqa.selenium.WebDriver;
import pageobjects.IFramePO;

public class IFrameTask {
    public WebDriver driver;
    private IFramePO element;

    public IFrameTask(WebDriver driver) {
        this.driver = driver;
        element = new IFramePO(driver);
    }

    public IFrameTask clickFormatsButton() {
        element.getFormatsButton().click();
        return this;
    }

    public String getTinyMCEText() {
        driver.switchTo().parentFrame();
        String text = element.getTinyMCETextField().getText();
        return text;
    }
}