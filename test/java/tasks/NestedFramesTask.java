package tasks;

import org.openqa.selenium.WebDriver;
import pageobjects.NestedFramesPO;

public class NestedFramesTask {
    public WebDriver driver;
    private NestedFramesPO element;

    public NestedFramesTask(WebDriver driver) {
        this.driver = driver;
        element = new NestedFramesPO(driver);
    }

    public String getFrameLeftText() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame("frame-left");
        String text = element.getFrameLeft().getText();
        return text;
    }

    public String getFrameMiddleText() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame("frame-middle");
        String text = element.getFrameMiddle().getText();
        return text;
    }

    public String getFrameRightText() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame("frame-right");
        String text = element.getFrameRight().getText();
        return text;
    }

    public String getFrameBottomText() {
        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-bottom");
        String text = element.getFrameBottom().getText();
        return text;
    }
}