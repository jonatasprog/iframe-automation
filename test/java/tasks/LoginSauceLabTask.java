package tasks;

import org.openqa.selenium.WebDriver;
import pageobjects.LoginSauceLabPO;

public class LoginSauceLabTask {
    public WebDriver driver;
    private LoginSauceLabPO element;

    public LoginSauceLabTask(WebDriver driver) {
        this.driver = driver;
        element = new LoginSauceLabPO(driver);
    }

    public LoginSauceLabTask fillUsernameTextField(String text) {
        element.setUsernameTextField().clear();
        element.setUsernameTextField().sendKeys(text);
        return this;
    }

    public LoginSauceLabTask fillPasswordTextField(String text) {
        element.setUsernameTextField().clear();
        element.setPasswordTextField().sendKeys(text);
        return this;
    }

    public LoginSauceLabTask clickLoginButton() {
        element.getLoginButton().click();
        return this;
    }
}
