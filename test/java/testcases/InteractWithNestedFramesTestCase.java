package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import support.BaseTest;
import tasks.NestedFramesTask;

public class InteractWithNestedFramesTestCase extends BaseTest {
    private static final String SYSTEM_URL = "http://the-internet.herokuapp.com/nested_frames";
    private WebDriver driver = getDriver();
    NestedFramesTask element = new NestedFramesTask(driver);

    @BeforeMethod
    public void setUp() {
        driver.get(SYSTEM_URL);
    }

    @Test
    public void InteractWithNestedFramesTest() {
        Assert.assertEquals("LEFT", element.getFrameLeftText());
        Assert.assertEquals("MIDDLE", element.getFrameMiddleText());
        Assert.assertEquals("RIGHT", element.getFrameRightText());
        Assert.assertEquals("BOTTOM", element.getFrameBottomText());
    }
}