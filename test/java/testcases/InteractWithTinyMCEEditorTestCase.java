package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import support.BaseTest;
import tasks.IFrameTask;

public class InteractWithTinyMCEEditorTestCase extends BaseTest {
    private static final String SYSTEM_URL = "http://the-internet.herokuapp.com/iframe";
    private WebDriver driver = getDriver();
    IFrameTask element = new IFrameTask(driver);

    @BeforeMethod
     public void setUp() {
        driver.get(SYSTEM_URL);
    }

    @Test
    public void interactWithTinyMCEEditorTest() {
        element.clickFormatsButton();
        Assert.assertEquals("An iFrame containing the TinyMCE WYSIWYG Editor", element.getTinyMCEText());
    }
}