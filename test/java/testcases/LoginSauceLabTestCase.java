package testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import support.BaseTest;
import tasks.LoginSauceLabTask;

public class LoginSauceLabTestCase extends BaseTest {
    private static final String SYSTEM_URL = "https://www.saucedemo.com/index.html";
    private WebDriver driver = getDriver();
    LoginSauceLabTask element = new LoginSauceLabTask(driver);

    @DataProvider(name = "TestData")
    public Object[][] getData(){
        Object[][] data = new Object[4][2];

        data[0][0]="standard_user";
        data[0][1]="secret_sauce";

        data[1][0]="locked_out_user";
        data[1][1]="secret_sauce";

        data[2][0]="problem_user";
        data[2][1]="secret_sauce";

        data[3][0]="performance_glitch_user";
        data[3][1]="secret_sauce";

        return data;
    }

    @BeforeMethod
    public void setUp() {
        driver.get(SYSTEM_URL);
    }

    @Test(dataProvider = "TestData")
    public void loginTest(String userName, String password) {
        element.fillUsernameTextField(userName);
        element.fillPasswordTextField(password);
        element.clickLoginButton();
    }
}
