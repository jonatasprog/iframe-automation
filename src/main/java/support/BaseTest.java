package support;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseTest {
    public WebDriver driver;
    public WebDriverWait wait;

   // @BeforeTest
//    public void setUp() {
//        System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
//        Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
//        WebDriverManager.chromedriver().setup();
//        this.driver = new ChromeDriver();
//        wait = new WebDriverWait(driver, 20);
//        driver.manage().window().maximize();
//    }

    public WebDriver getDriver() {
        this.driver = Browsers.setDriver(DriverTypeFw.CHROME);
        return driver;
    }

    @AfterClass
    public void tearDown() {
        Browsers.quitDriver();
    }
}
