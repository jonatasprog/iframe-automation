package support;

public enum DriverTypeFw {
    CHROME,
    FIREFOX,
    IE,
    CHROME_HEADLESS;
}
