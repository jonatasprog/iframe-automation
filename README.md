# IFrame-Automation

## About 
Simple **web automation project** with different domains to practice test automation from scratch working with a bunch of elements, including Iframes.  

## Getting Started

### Dependencies

* [Java](https://www.java.com/) programming language. Class-based, object-oriented, and designed to have as few implementation dependencies as possible.
* [Maven](https://maven.apache.org/) project dependency management. It can manage a project's build, reporting and documentation from a central piece of information.
* [Selenium WebDriver](https://www.seleniumhq.org/) automation framework. Wich is a collection of open source APIs which are used to automate the testing of a web application. This tool is used to automate web application testing to verify that it works as expected. It supports many browsers such as Firefox, Chrome, IE, and Safari.
* [TestNG](https://testng.org/) testing framework. Designed to simplify a broad range of testing needs, from unit testing (testing a class in isolation of the others) to integration testing (testing entire systems made of several classes, several packages and even several external frameworks, such as application servers).
* [WebDriverManager](https://github.com/bonigarcia/webdrivermanager) library. It allows to automate the management of the binary drivers (e.g. chromedriver, geckodriver, etc.) required by Selenium.

### Requirements to install

* IntelliJ, Eclipse or your preferred IDE.
* Java SE Development Kit 8.
* Apache Maven in your machine as long as your IDE couldn't convert automatically to a maven project using pom.xml.

<p>**Intalling Apache Maven**: <link>https://maven.apache.org/install.html</link></p>
<p>**Converting Existing Java Project to Maven (Eclipse)**: <link>https://crunchify.com/how-to-convert-existing-java-project-to-maven-in-eclipse/</link></p>
<p>**Converting Existing Java Project to Maven (IntelliJ)**: <link>https://www.jetbrains.com/help/idea/convert-a-regular-project-into-a-maven-project.html/</link></p>
<p>**Converting Existing Java Project to Maven (NetBeans)**: <link>https://platform.netbeans.org/tutorials/71/nbm-maven-quickstart.html/</link></p>

### Executing program

* Download or clone the project to your computer then import It from your IDE as maven project. Build and run the test Cases chosen or run regression.xml to run every tests.

### Application domains
* http://the-internet.herokuapp.com/iframe
* http://the-internet.herokuapp.com/nested_frames
* https://www.saucedemo.com/index.html

## Authors

*Jônatas Deorce*  
@jonatasprog